================================================================================
HPLinpack 2.3  --  High-Performance Linpack benchmark  --   December 2, 2018
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :  163840        0        0 
NB     :     256 
PMAP   : Row-major process mapping
P      :       7 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Right 
BCAST  :   1ring 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               2.220446e-16
- Computational tests pass if scaled residuals are less than                16.0

Column=000000256 Fraction= 0.2% Gflops=2.673e+03
Column=000000512 Fraction= 0.3% Gflops=1.868e+03
Column=000000768 Fraction= 0.5% Gflops=1.967e+03
Column=000001024 Fraction= 0.6% Gflops=1.984e+03
Column=000001280 Fraction= 0.8% Gflops=1.994e+03
Column=000001536 Fraction= 0.9% Gflops=2.000e+03
Column=000001792 Fraction= 1.1% Gflops=2.002e+03
Column=000002048 Fraction= 1.2% Gflops=2.005e+03
Column=000002304 Fraction= 1.4% Gflops=2.010e+03
Column=000002560 Fraction= 1.6% Gflops=1.998e+03
Column=000002816 Fraction= 1.7% Gflops=2.000e+03
Column=000003072 Fraction= 1.9% Gflops=2.002e+03
Column=000003328 Fraction= 2.0% Gflops=2.002e+03
Column=000003584 Fraction= 2.2% Gflops=2.003e+03
Column=000003840 Fraction= 2.3% Gflops=2.004e+03
Column=000004096 Fraction= 2.5% Gflops=2.005e+03
Column=000004352 Fraction= 2.7% Gflops=2.007e+03
Column=000004608 Fraction= 2.8% Gflops=2.000e+03
Column=000004864 Fraction= 3.0% Gflops=2.001e+03
Column=000005120 Fraction= 3.1% Gflops=2.001e+03
Column=000005376 Fraction= 3.3% Gflops=2.002e+03
Column=000005632 Fraction= 3.4% Gflops=2.002e+03
Column=000005888 Fraction= 3.6% Gflops=2.003e+03
Column=000006144 Fraction= 3.8% Gflops=2.002e+03
Column=000006400 Fraction= 3.9% Gflops=2.004e+03
Column=000006656 Fraction= 4.1% Gflops=2.000e+03
Column=000006912 Fraction= 4.2% Gflops=2.001e+03
Column=000007168 Fraction= 4.4% Gflops=2.002e+03
Column=000007424 Fraction= 4.5% Gflops=2.003e+03
Column=000007680 Fraction= 4.7% Gflops=2.003e+03
Column=000007936 Fraction= 4.8% Gflops=2.004e+03
Column=000008192 Fraction= 5.0% Gflops=2.005e+03
Column=000008448 Fraction= 5.2% Gflops=2.006e+03
Column=000008704 Fraction= 5.3% Gflops=2.004e+03
Column=000008960 Fraction= 5.5% Gflops=2.004e+03
Column=000009216 Fraction= 5.6% Gflops=2.004e+03
Column=000009472 Fraction= 5.8% Gflops=2.005e+03
Column=000009728 Fraction= 5.9% Gflops=2.004e+03
Column=000009984 Fraction= 6.1% Gflops=2.004e+03
Column=000010240 Fraction= 6.2% Gflops=2.004e+03
Column=000010496 Fraction= 6.4% Gflops=2.005e+03
Column=000010752 Fraction= 6.6% Gflops=2.003e+03
Column=000011008 Fraction= 6.7% Gflops=2.002e+03
Column=000011264 Fraction= 6.9% Gflops=2.002e+03
Column=000011520 Fraction= 7.0% Gflops=2.002e+03
Column=000011776 Fraction= 7.2% Gflops=2.001e+03
Column=000012032 Fraction= 7.3% Gflops=2.001e+03
Column=000012288 Fraction= 7.5% Gflops=2.001e+03
Column=000012544 Fraction= 7.7% Gflops=2.001e+03
Column=000012800 Fraction= 7.8% Gflops=1.998e+03
Column=000013056 Fraction= 8.0% Gflops=1.998e+03
Column=000013312 Fraction= 8.1% Gflops=1.997e+03
Column=000013568 Fraction= 8.3% Gflops=1.997e+03
Column=000013824 Fraction= 8.4% Gflops=1.997e+03
Column=000014080 Fraction= 8.6% Gflops=1.997e+03
Column=000014336 Fraction= 8.8% Gflops=1.997e+03
Column=000014592 Fraction= 8.9% Gflops=1.998e+03
Column=000014848 Fraction= 9.1% Gflops=1.996e+03
Column=000015104 Fraction= 9.2% Gflops=1.996e+03
Column=000015360 Fraction= 9.4% Gflops=1.996e+03
Column=000015616 Fraction= 9.5% Gflops=1.997e+03
Column=000015872 Fraction= 9.7% Gflops=1.997e+03
Column=000016128 Fraction= 9.8% Gflops=1.998e+03
Column=000016384 Fraction=10.0% Gflops=1.998e+03
Column=000016640 Fraction=10.2% Gflops=1.999e+03
Column=000016896 Fraction=10.3% Gflops=1.998e+03
Column=000017152 Fraction=10.5% Gflops=1.998e+03
Column=000017408 Fraction=10.6% Gflops=1.999e+03
Column=000017664 Fraction=10.8% Gflops=1.999e+03
Column=000017920 Fraction=10.9% Gflops=1.999e+03
Column=000018176 Fraction=11.1% Gflops=1.999e+03
Column=000018432 Fraction=11.2% Gflops=1.999e+03
Column=000018688 Fraction=11.4% Gflops=1.999e+03
Column=000018944 Fraction=11.6% Gflops=1.997e+03
Column=000019200 Fraction=11.7% Gflops=1.997e+03
Column=000019456 Fraction=11.9% Gflops=1.997e+03
Column=000019712 Fraction=12.0% Gflops=1.997e+03
Column=000019968 Fraction=12.2% Gflops=1.997e+03
Column=000020224 Fraction=12.3% Gflops=1.996e+03
Column=000020480 Fraction=12.5% Gflops=1.996e+03
Column=000020736 Fraction=12.7% Gflops=1.996e+03
Column=000020992 Fraction=12.8% Gflops=1.994e+03
Column=000021248 Fraction=13.0% Gflops=1.994e+03
Column=000021504 Fraction=13.1% Gflops=1.993e+03
Column=000021760 Fraction=13.3% Gflops=1.993e+03
Column=000022016 Fraction=13.4% Gflops=1.993e+03
Column=000022272 Fraction=13.6% Gflops=1.993e+03
Column=000022528 Fraction=13.8% Gflops=1.992e+03
Column=000022784 Fraction=13.9% Gflops=1.993e+03
Column=000023040 Fraction=14.1% Gflops=1.992e+03
Column=000023296 Fraction=14.2% Gflops=1.991e+03
Column=000023552 Fraction=14.4% Gflops=1.991e+03
Column=000023808 Fraction=14.5% Gflops=1.991e+03
Column=000024064 Fraction=14.7% Gflops=1.991e+03
Column=000024320 Fraction=14.8% Gflops=1.992e+03
Column=000024576 Fraction=15.0% Gflops=1.992e+03
Column=000024832 Fraction=15.2% Gflops=1.992e+03
Column=000025088 Fraction=15.3% Gflops=1.991e+03
Column=000025344 Fraction=15.5% Gflops=1.992e+03
Column=000025600 Fraction=15.6% Gflops=1.992e+03
Column=000025856 Fraction=15.8% Gflops=1.992e+03
Column=000026112 Fraction=15.9% Gflops=1.992e+03
Column=000026368 Fraction=16.1% Gflops=1.993e+03
Column=000026624 Fraction=16.2% Gflops=1.993e+03
Column=000026880 Fraction=16.4% Gflops=1.994e+03
Column=000027136 Fraction=16.6% Gflops=1.993e+03
Column=000027392 Fraction=16.7% Gflops=1.993e+03
Column=000027648 Fraction=16.9% Gflops=1.992e+03
Column=000027904 Fraction=17.0% Gflops=1.992e+03
Column=000028160 Fraction=17.2% Gflops=1.992e+03
Column=000028416 Fraction=17.3% Gflops=1.992e+03
Column=000028672 Fraction=17.5% Gflops=1.992e+03
Column=000028928 Fraction=17.7% Gflops=1.992e+03
Column=000029184 Fraction=17.8% Gflops=1.992e+03
Column=000029440 Fraction=18.0% Gflops=1.992e+03
Column=000029696 Fraction=18.1% Gflops=1.992e+03
Column=000029952 Fraction=18.3% Gflops=1.992e+03
Column=000030208 Fraction=18.4% Gflops=1.993e+03
Column=000030464 Fraction=18.6% Gflops=1.993e+03
Column=000030720 Fraction=18.8% Gflops=1.993e+03
Column=000030976 Fraction=18.9% Gflops=1.993e+03
Column=000031232 Fraction=19.1% Gflops=1.992e+03
Column=000031488 Fraction=19.2% Gflops=1.992e+03
Column=000031744 Fraction=19.4% Gflops=1.992e+03
Column=000032000 Fraction=19.5% Gflops=1.992e+03
Column=000032256 Fraction=19.7% Gflops=1.992e+03
Column=000032512 Fraction=19.8% Gflops=1.992e+03
Column=000032768 Fraction=20.0% Gflops=1.993e+03
Column=000033024 Fraction=20.2% Gflops=1.993e+03
Column=000033280 Fraction=20.3% Gflops=1.992e+03
Column=000033536 Fraction=20.5% Gflops=1.992e+03
Column=000033792 Fraction=20.6% Gflops=1.993e+03
Column=000034048 Fraction=20.8% Gflops=1.993e+03
Column=000034304 Fraction=20.9% Gflops=1.993e+03
Column=000034560 Fraction=21.1% Gflops=1.993e+03
Column=000034816 Fraction=21.2% Gflops=1.993e+03
Column=000035072 Fraction=21.4% Gflops=1.993e+03
Column=000035328 Fraction=21.6% Gflops=1.992e+03
Column=000035584 Fraction=21.7% Gflops=1.992e+03
Column=000035840 Fraction=21.9% Gflops=1.992e+03
Column=000036096 Fraction=22.0% Gflops=1.992e+03
Column=000036352 Fraction=22.2% Gflops=1.992e+03
Column=000036608 Fraction=22.3% Gflops=1.991e+03
Column=000036864 Fraction=22.5% Gflops=1.991e+03
Column=000037120 Fraction=22.7% Gflops=1.992e+03
Column=000037376 Fraction=22.8% Gflops=1.991e+03
Column=000037632 Fraction=23.0% Gflops=1.991e+03
Column=000037888 Fraction=23.1% Gflops=1.991e+03
Column=000038144 Fraction=23.3% Gflops=1.991e+03
Column=000038400 Fraction=23.4% Gflops=1.991e+03
Column=000038656 Fraction=23.6% Gflops=1.990e+03
Column=000038912 Fraction=23.8% Gflops=1.990e+03
Column=000039168 Fraction=23.9% Gflops=1.990e+03
Column=000039424 Fraction=24.1% Gflops=1.990e+03
Column=000039680 Fraction=24.2% Gflops=1.990e+03
Column=000039936 Fraction=24.4% Gflops=1.990e+03
Column=000040192 Fraction=24.5% Gflops=1.990e+03
Column=000040448 Fraction=24.7% Gflops=1.990e+03
Column=000040704 Fraction=24.8% Gflops=1.990e+03
Column=000040960 Fraction=25.0% Gflops=1.990e+03
Column=000041216 Fraction=25.2% Gflops=1.991e+03
Column=000041472 Fraction=25.3% Gflops=1.990e+03
Column=000041728 Fraction=25.5% Gflops=1.991e+03
Column=000041984 Fraction=25.6% Gflops=1.991e+03
Column=000042240 Fraction=25.8% Gflops=1.991e+03
Column=000042496 Fraction=25.9% Gflops=1.991e+03
Column=000042752 Fraction=26.1% Gflops=1.991e+03
Column=000043008 Fraction=26.2% Gflops=1.990e+03
Column=000043264 Fraction=26.4% Gflops=1.991e+03
Column=000043520 Fraction=26.6% Gflops=1.990e+03
Column=000043776 Fraction=26.7% Gflops=1.990e+03
Column=000044032 Fraction=26.9% Gflops=1.990e+03
Column=000044288 Fraction=27.0% Gflops=1.990e+03
Column=000044544 Fraction=27.2% Gflops=1.990e+03
Column=000044800 Fraction=27.3% Gflops=1.990e+03
Column=000045056 Fraction=27.5% Gflops=1.990e+03
Column=000045312 Fraction=27.7% Gflops=1.990e+03
Column=000045568 Fraction=27.8% Gflops=1.989e+03
Column=000045824 Fraction=28.0% Gflops=1.989e+03
Column=000046080 Fraction=28.1% Gflops=1.988e+03
Column=000046336 Fraction=28.3% Gflops=1.988e+03
Column=000046592 Fraction=28.4% Gflops=1.988e+03
Column=000046848 Fraction=28.6% Gflops=1.988e+03
Column=000047104 Fraction=28.8% Gflops=1.988e+03
Column=000047360 Fraction=28.9% Gflops=1.988e+03
Column=000047616 Fraction=29.1% Gflops=1.987e+03
Column=000047872 Fraction=29.2% Gflops=1.988e+03
Column=000048128 Fraction=29.4% Gflops=1.988e+03
Column=000048384 Fraction=29.5% Gflops=1.988e+03
Column=000048640 Fraction=29.7% Gflops=1.988e+03
Column=000048896 Fraction=29.8% Gflops=1.988e+03
Column=000049152 Fraction=30.0% Gflops=1.988e+03
Column=000049408 Fraction=30.2% Gflops=1.988e+03
Column=000049664 Fraction=30.3% Gflops=1.987e+03
Column=000049920 Fraction=30.5% Gflops=1.987e+03
Column=000050176 Fraction=30.6% Gflops=1.987e+03
Column=000050432 Fraction=30.8% Gflops=1.987e+03
Column=000050688 Fraction=30.9% Gflops=1.987e+03
Column=000050944 Fraction=31.1% Gflops=1.987e+03
Column=000051200 Fraction=31.2% Gflops=1.987e+03
Column=000051456 Fraction=31.4% Gflops=1.987e+03
Column=000051712 Fraction=31.6% Gflops=1.987e+03
Column=000051968 Fraction=31.7% Gflops=1.987e+03
Column=000052224 Fraction=31.9% Gflops=1.987e+03
Column=000052480 Fraction=32.0% Gflops=1.987e+03
Column=000052736 Fraction=32.2% Gflops=1.986e+03
Column=000052992 Fraction=32.3% Gflops=1.986e+03
Column=000053248 Fraction=32.5% Gflops=1.986e+03
Column=000053504 Fraction=32.7% Gflops=1.987e+03
Column=000053760 Fraction=32.8% Gflops=1.986e+03
Column=000054016 Fraction=33.0% Gflops=1.986e+03
Column=000054272 Fraction=33.1% Gflops=1.986e+03
Column=000054528 Fraction=33.3% Gflops=1.986e+03
Column=000054784 Fraction=33.4% Gflops=1.986e+03
Column=000055040 Fraction=33.6% Gflops=1.986e+03
Column=000055296 Fraction=33.8% Gflops=1.986e+03
Column=000055552 Fraction=33.9% Gflops=1.986e+03
Column=000055808 Fraction=34.1% Gflops=1.985e+03
Column=000056064 Fraction=34.2% Gflops=1.985e+03
Column=000056320 Fraction=34.4% Gflops=1.985e+03
Column=000056576 Fraction=34.5% Gflops=1.985e+03
Column=000056832 Fraction=34.7% Gflops=1.985e+03
Column=000057088 Fraction=34.8% Gflops=1.985e+03
Column=000057344 Fraction=35.0% Gflops=1.985e+03
Column=000057600 Fraction=35.2% Gflops=1.985e+03
Column=000057856 Fraction=35.3% Gflops=1.985e+03
Column=000058112 Fraction=35.5% Gflops=1.985e+03
Column=000058368 Fraction=35.6% Gflops=1.985e+03
Column=000058624 Fraction=35.8% Gflops=1.984e+03
Column=000058880 Fraction=35.9% Gflops=1.984e+03
Column=000059136 Fraction=36.1% Gflops=1.984e+03
Column=000059392 Fraction=36.2% Gflops=1.984e+03
Column=000059648 Fraction=36.4% Gflops=1.985e+03
Column=000059904 Fraction=36.6% Gflops=1.984e+03
Column=000060160 Fraction=36.7% Gflops=1.984e+03
Column=000060416 Fraction=36.9% Gflops=1.984e+03
Column=000060672 Fraction=37.0% Gflops=1.985e+03
Column=000060928 Fraction=37.2% Gflops=1.985e+03
Column=000061184 Fraction=37.3% Gflops=1.985e+03
Column=000061440 Fraction=37.5% Gflops=1.985e+03
Column=000061696 Fraction=37.7% Gflops=1.985e+03
Column=000061952 Fraction=37.8% Gflops=1.984e+03
Column=000062208 Fraction=38.0% Gflops=1.984e+03
Column=000062464 Fraction=38.1% Gflops=1.984e+03
Column=000062720 Fraction=38.3% Gflops=1.984e+03
Column=000062976 Fraction=38.4% Gflops=1.984e+03
Column=000063232 Fraction=38.6% Gflops=1.984e+03
Column=000063488 Fraction=38.8% Gflops=1.984e+03
Column=000063744 Fraction=38.9% Gflops=1.984e+03
Column=000064000 Fraction=39.1% Gflops=1.984e+03
Column=000064256 Fraction=39.2% Gflops=1.984e+03
Column=000064512 Fraction=39.4% Gflops=1.984e+03
Column=000064768 Fraction=39.5% Gflops=1.985e+03
Column=000065024 Fraction=39.7% Gflops=1.985e+03
Column=000065280 Fraction=39.8% Gflops=1.985e+03
Column=000065536 Fraction=40.0% Gflops=1.985e+03
Column=000065792 Fraction=40.2% Gflops=1.985e+03
Column=000066048 Fraction=40.3% Gflops=1.985e+03
Column=000066304 Fraction=40.5% Gflops=1.985e+03
Column=000066560 Fraction=40.6% Gflops=1.985e+03
Column=000066816 Fraction=40.8% Gflops=1.985e+03
Column=000067072 Fraction=40.9% Gflops=1.985e+03
Column=000067328 Fraction=41.1% Gflops=1.985e+03
Column=000067584 Fraction=41.2% Gflops=1.985e+03
Column=000067840 Fraction=41.4% Gflops=1.985e+03
Column=000068096 Fraction=41.6% Gflops=1.985e+03
Column=000068352 Fraction=41.7% Gflops=1.985e+03
Column=000068608 Fraction=41.9% Gflops=1.985e+03
Column=000068864 Fraction=42.0% Gflops=1.985e+03
Column=000069120 Fraction=42.2% Gflops=1.985e+03
Column=000069376 Fraction=42.3% Gflops=1.985e+03
Column=000069632 Fraction=42.5% Gflops=1.985e+03
Column=000069888 Fraction=42.7% Gflops=1.985e+03
Column=000070144 Fraction=42.8% Gflops=1.985e+03
Column=000070400 Fraction=43.0% Gflops=1.985e+03
Column=000070656 Fraction=43.1% Gflops=1.984e+03
Column=000070912 Fraction=43.3% Gflops=1.984e+03
Column=000071168 Fraction=43.4% Gflops=1.984e+03
Column=000071424 Fraction=43.6% Gflops=1.984e+03
Column=000071680 Fraction=43.8% Gflops=1.984e+03
Column=000071936 Fraction=43.9% Gflops=1.984e+03
Column=000072192 Fraction=44.1% Gflops=1.984e+03
Column=000072448 Fraction=44.2% Gflops=1.984e+03
Column=000072704 Fraction=44.4% Gflops=1.984e+03
Column=000072960 Fraction=44.5% Gflops=1.984e+03
Column=000073216 Fraction=44.7% Gflops=1.984e+03
Column=000073472 Fraction=44.8% Gflops=1.984e+03
Column=000073728 Fraction=45.0% Gflops=1.984e+03
Column=000073984 Fraction=45.2% Gflops=1.984e+03
Column=000074240 Fraction=45.3% Gflops=1.984e+03
Column=000074496 Fraction=45.5% Gflops=1.984e+03
Column=000074752 Fraction=45.6% Gflops=1.984e+03
Column=000075008 Fraction=45.8% Gflops=1.984e+03
Column=000075264 Fraction=45.9% Gflops=1.984e+03
Column=000075520 Fraction=46.1% Gflops=1.984e+03
Column=000075776 Fraction=46.2% Gflops=1.984e+03
Column=000076032 Fraction=46.4% Gflops=1.984e+03
Column=000076288 Fraction=46.6% Gflops=1.984e+03
Column=000076544 Fraction=46.7% Gflops=1.984e+03
Column=000076800 Fraction=46.9% Gflops=1.984e+03
Column=000077056 Fraction=47.0% Gflops=1.984e+03
Column=000077312 Fraction=47.2% Gflops=1.984e+03
Column=000077568 Fraction=47.3% Gflops=1.984e+03
Column=000077824 Fraction=47.5% Gflops=1.984e+03
Column=000078080 Fraction=47.7% Gflops=1.984e+03
Column=000078336 Fraction=47.8% Gflops=1.983e+03
Column=000078592 Fraction=48.0% Gflops=1.983e+03
Column=000078848 Fraction=48.1% Gflops=1.983e+03
Column=000079104 Fraction=48.3% Gflops=1.983e+03
Column=000079360 Fraction=48.4% Gflops=1.983e+03
Column=000079616 Fraction=48.6% Gflops=1.983e+03
Column=000079872 Fraction=48.8% Gflops=1.983e+03
Column=000080128 Fraction=48.9% Gflops=1.983e+03
Column=000080384 Fraction=49.1% Gflops=1.983e+03
Column=000080640 Fraction=49.2% Gflops=1.983e+03
Column=000080896 Fraction=49.4% Gflops=1.983e+03
Column=000081152 Fraction=49.5% Gflops=1.983e+03
Column=000081408 Fraction=49.7% Gflops=1.983e+03
Column=000081664 Fraction=49.8% Gflops=1.983e+03
Column=000081920 Fraction=50.0% Gflops=1.983e+03
Column=000082176 Fraction=50.2% Gflops=1.983e+03
Column=000082432 Fraction=50.3% Gflops=1.983e+03
Column=000082688 Fraction=50.5% Gflops=1.983e+03
Column=000082944 Fraction=50.6% Gflops=1.982e+03
Column=000083200 Fraction=50.8% Gflops=1.982e+03
Column=000083456 Fraction=50.9% Gflops=1.982e+03
Column=000083712 Fraction=51.1% Gflops=1.982e+03
Column=000083968 Fraction=51.2% Gflops=1.982e+03
Column=000084224 Fraction=51.4% Gflops=1.982e+03
Column=000084480 Fraction=51.6% Gflops=1.982e+03
Column=000084736 Fraction=51.7% Gflops=1.982e+03
Column=000084992 Fraction=51.9% Gflops=1.982e+03
Column=000085248 Fraction=52.0% Gflops=1.982e+03
Column=000085504 Fraction=52.2% Gflops=1.982e+03
Column=000085760 Fraction=52.3% Gflops=1.982e+03
Column=000086016 Fraction=52.5% Gflops=1.981e+03
Column=000086272 Fraction=52.7% Gflops=1.981e+03
Column=000086528 Fraction=52.8% Gflops=1.981e+03
Column=000086784 Fraction=53.0% Gflops=1.981e+03
Column=000087040 Fraction=53.1% Gflops=1.981e+03
Column=000087296 Fraction=53.3% Gflops=1.981e+03
Column=000087552 Fraction=53.4% Gflops=1.981e+03
Column=000087808 Fraction=53.6% Gflops=1.981e+03
Column=000088064 Fraction=53.8% Gflops=1.981e+03
Column=000088320 Fraction=53.9% Gflops=1.981e+03
Column=000088576 Fraction=54.1% Gflops=1.980e+03
Column=000088832 Fraction=54.2% Gflops=1.980e+03
Column=000089088 Fraction=54.4% Gflops=1.980e+03
Column=000089344 Fraction=54.5% Gflops=1.980e+03
Column=000089600 Fraction=54.7% Gflops=1.980e+03
Column=000089856 Fraction=54.8% Gflops=1.980e+03
Column=000090112 Fraction=55.0% Gflops=1.980e+03
Column=000090368 Fraction=55.2% Gflops=1.980e+03
Column=000090624 Fraction=55.3% Gflops=1.980e+03
Column=000090880 Fraction=55.5% Gflops=1.980e+03
Column=000091136 Fraction=55.6% Gflops=1.980e+03
Column=000091392 Fraction=55.8% Gflops=1.980e+03
Column=000091648 Fraction=55.9% Gflops=1.980e+03
Column=000091904 Fraction=56.1% Gflops=1.980e+03
Column=000092160 Fraction=56.2% Gflops=1.980e+03
Column=000092416 Fraction=56.4% Gflops=1.979e+03
Column=000092672 Fraction=56.6% Gflops=1.979e+03
Column=000092928 Fraction=56.7% Gflops=1.979e+03
Column=000093184 Fraction=56.9% Gflops=1.979e+03
Column=000093440 Fraction=57.0% Gflops=1.979e+03
Column=000093696 Fraction=57.2% Gflops=1.979e+03
Column=000093952 Fraction=57.3% Gflops=1.979e+03
Column=000094208 Fraction=57.5% Gflops=1.979e+03
Column=000094464 Fraction=57.7% Gflops=1.979e+03
Column=000094720 Fraction=57.8% Gflops=1.978e+03
Column=000094976 Fraction=58.0% Gflops=1.978e+03
Column=000095232 Fraction=58.1% Gflops=1.978e+03
Column=000095488 Fraction=58.3% Gflops=1.978e+03
Column=000095744 Fraction=58.4% Gflops=1.978e+03
Column=000096000 Fraction=58.6% Gflops=1.978e+03
Column=000096256 Fraction=58.8% Gflops=1.978e+03
Column=000096512 Fraction=58.9% Gflops=1.978e+03
Column=000096768 Fraction=59.1% Gflops=1.978e+03
Column=000097024 Fraction=59.2% Gflops=1.978e+03
Column=000097280 Fraction=59.4% Gflops=1.978e+03
Column=000097536 Fraction=59.5% Gflops=1.978e+03
Column=000097792 Fraction=59.7% Gflops=1.978e+03
Column=000098048 Fraction=59.8% Gflops=1.978e+03
Column=000098304 Fraction=60.0% Gflops=1.978e+03
Column=000098560 Fraction=60.2% Gflops=1.978e+03
Column=000098816 Fraction=60.3% Gflops=1.978e+03
Column=000099072 Fraction=60.5% Gflops=1.978e+03
Column=000099328 Fraction=60.6% Gflops=1.978e+03
Column=000099584 Fraction=60.8% Gflops=1.978e+03
Column=000099840 Fraction=60.9% Gflops=1.978e+03
Column=000100096 Fraction=61.1% Gflops=1.978e+03
Column=000100352 Fraction=61.2% Gflops=1.978e+03
Column=000100608 Fraction=61.4% Gflops=1.978e+03
Column=000100864 Fraction=61.6% Gflops=1.978e+03
Column=000101120 Fraction=61.7% Gflops=1.977e+03
Column=000101376 Fraction=61.9% Gflops=1.977e+03
Column=000101632 Fraction=62.0% Gflops=1.977e+03
Column=000101888 Fraction=62.2% Gflops=1.977e+03
Column=000102144 Fraction=62.3% Gflops=1.977e+03
Column=000102400 Fraction=62.5% Gflops=1.977e+03
Column=000102656 Fraction=62.7% Gflops=1.977e+03
Column=000102912 Fraction=62.8% Gflops=1.977e+03
Column=000103168 Fraction=63.0% Gflops=1.977e+03
Column=000103424 Fraction=63.1% Gflops=1.977e+03
Column=000103680 Fraction=63.3% Gflops=1.977e+03
Column=000103936 Fraction=63.4% Gflops=1.977e+03
Column=000104192 Fraction=63.6% Gflops=1.977e+03
Column=000104448 Fraction=63.8% Gflops=1.977e+03
Column=000104704 Fraction=63.9% Gflops=1.977e+03
Column=000104960 Fraction=64.1% Gflops=1.977e+03
Column=000105216 Fraction=64.2% Gflops=1.977e+03
Column=000105472 Fraction=64.4% Gflops=1.977e+03
Column=000105728 Fraction=64.5% Gflops=1.977e+03
Column=000105984 Fraction=64.7% Gflops=1.977e+03
Column=000106240 Fraction=64.8% Gflops=1.977e+03
Column=000106496 Fraction=65.0% Gflops=1.977e+03
Column=000106752 Fraction=65.2% Gflops=1.977e+03
Column=000107008 Fraction=65.3% Gflops=1.977e+03
Column=000107264 Fraction=65.5% Gflops=1.977e+03
Column=000107520 Fraction=65.6% Gflops=1.976e+03
Column=000107776 Fraction=65.8% Gflops=1.976e+03
Column=000108032 Fraction=65.9% Gflops=1.976e+03
Column=000108288 Fraction=66.1% Gflops=1.976e+03
Column=000108544 Fraction=66.2% Gflops=1.976e+03
Column=000108800 Fraction=66.4% Gflops=1.976e+03
Column=000109056 Fraction=66.6% Gflops=1.976e+03
Column=000109312 Fraction=66.7% Gflops=1.976e+03
Column=000109568 Fraction=66.9% Gflops=1.976e+03
Column=000109824 Fraction=67.0% Gflops=1.976e+03
Column=000110080 Fraction=67.2% Gflops=1.976e+03
Column=000110336 Fraction=67.3% Gflops=1.976e+03
Column=000110592 Fraction=67.5% Gflops=1.976e+03
Column=000110848 Fraction=67.7% Gflops=1.976e+03
Column=000111104 Fraction=67.8% Gflops=1.976e+03
Column=000111360 Fraction=68.0% Gflops=1.976e+03
Column=000111616 Fraction=68.1% Gflops=1.976e+03
Column=000111872 Fraction=68.3% Gflops=1.975e+03
Column=000112128 Fraction=68.4% Gflops=1.975e+03
Column=000112384 Fraction=68.6% Gflops=1.975e+03
Column=000112640 Fraction=68.8% Gflops=1.975e+03
Column=000112896 Fraction=68.9% Gflops=1.975e+03
Column=000113152 Fraction=69.1% Gflops=1.975e+03
Column=000113408 Fraction=69.2% Gflops=1.975e+03
Column=000113664 Fraction=69.4% Gflops=1.975e+03
Column=000113920 Fraction=69.5% Gflops=1.975e+03
Column=000114176 Fraction=69.7% Gflops=1.975e+03
Column=000114432 Fraction=69.8% Gflops=1.975e+03
Column=000114688 Fraction=70.0% Gflops=1.975e+03
Column=000114944 Fraction=70.2% Gflops=1.975e+03
Column=000115200 Fraction=70.3% Gflops=1.975e+03
Column=000115456 Fraction=70.5% Gflops=1.975e+03
Column=000115712 Fraction=70.6% Gflops=1.975e+03
Column=000115968 Fraction=70.8% Gflops=1.975e+03
Column=000116224 Fraction=70.9% Gflops=1.975e+03
Column=000116480 Fraction=71.1% Gflops=1.974e+03
Column=000116736 Fraction=71.2% Gflops=1.974e+03
Column=000116992 Fraction=71.4% Gflops=1.974e+03
Column=000117248 Fraction=71.6% Gflops=1.974e+03
Column=000117504 Fraction=71.7% Gflops=1.974e+03
Column=000117760 Fraction=71.9% Gflops=1.974e+03
Column=000118016 Fraction=72.0% Gflops=1.974e+03
Column=000118272 Fraction=72.2% Gflops=1.974e+03
Column=000118528 Fraction=72.3% Gflops=1.974e+03
Column=000118784 Fraction=72.5% Gflops=1.974e+03
Column=000119040 Fraction=72.7% Gflops=1.974e+03
Column=000119296 Fraction=72.8% Gflops=1.974e+03
Column=000119552 Fraction=73.0% Gflops=1.974e+03
Column=000119808 Fraction=73.1% Gflops=1.974e+03
Column=000120064 Fraction=73.3% Gflops=1.974e+03
Column=000120320 Fraction=73.4% Gflops=1.973e+03
Column=000120576 Fraction=73.6% Gflops=1.973e+03
Column=000120832 Fraction=73.8% Gflops=1.973e+03
Column=000121088 Fraction=73.9% Gflops=1.973e+03
Column=000121344 Fraction=74.1% Gflops=1.973e+03
Column=000121600 Fraction=74.2% Gflops=1.973e+03
Column=000121856 Fraction=74.4% Gflops=1.973e+03
Column=000122112 Fraction=74.5% Gflops=1.973e+03
Column=000122368 Fraction=74.7% Gflops=1.973e+03
Column=000122624 Fraction=74.8% Gflops=1.973e+03
Column=000122880 Fraction=75.0% Gflops=1.973e+03
Column=000123136 Fraction=75.2% Gflops=1.973e+03
Column=000123392 Fraction=75.3% Gflops=1.973e+03
Column=000123648 Fraction=75.5% Gflops=1.973e+03
Column=000123904 Fraction=75.6% Gflops=1.973e+03
Column=000124160 Fraction=75.8% Gflops=1.973e+03
Column=000124416 Fraction=75.9% Gflops=1.972e+03
Column=000124672 Fraction=76.1% Gflops=1.972e+03
Column=000124928 Fraction=76.2% Gflops=1.972e+03
Column=000125184 Fraction=76.4% Gflops=1.972e+03
Column=000125440 Fraction=76.6% Gflops=1.972e+03
Column=000125696 Fraction=76.7% Gflops=1.972e+03
Column=000125952 Fraction=76.9% Gflops=1.972e+03
Column=000126208 Fraction=77.0% Gflops=1.972e+03
Column=000126464 Fraction=77.2% Gflops=1.972e+03
Column=000126720 Fraction=77.3% Gflops=1.972e+03
Column=000126976 Fraction=77.5% Gflops=1.972e+03
Column=000127232 Fraction=77.7% Gflops=1.972e+03
Column=000127488 Fraction=77.8% Gflops=1.972e+03
Column=000127744 Fraction=78.0% Gflops=1.972e+03
Column=000128000 Fraction=78.1% Gflops=1.972e+03
Column=000128256 Fraction=78.3% Gflops=1.971e+03
Column=000128512 Fraction=78.4% Gflops=1.971e+03
Column=000128768 Fraction=78.6% Gflops=1.971e+03
Column=000129024 Fraction=78.8% Gflops=1.971e+03
Column=000129280 Fraction=78.9% Gflops=1.971e+03
Column=000129536 Fraction=79.1% Gflops=1.971e+03
Column=000129792 Fraction=79.2% Gflops=1.971e+03
Column=000130048 Fraction=79.4% Gflops=1.971e+03
Column=000130304 Fraction=79.5% Gflops=1.971e+03
Column=000130560 Fraction=79.7% Gflops=1.971e+03
Column=000130816 Fraction=79.8% Gflops=1.971e+03
Column=000131072 Fraction=80.0% Gflops=1.971e+03
Column=000131328 Fraction=80.2% Gflops=1.971e+03
Column=000131584 Fraction=80.3% Gflops=1.971e+03
Column=000131840 Fraction=80.5% Gflops=1.971e+03
Column=000132096 Fraction=80.6% Gflops=1.971e+03
Column=000132352 Fraction=80.8% Gflops=1.971e+03
Column=000132608 Fraction=80.9% Gflops=1.971e+03
Column=000132864 Fraction=81.1% Gflops=1.971e+03
Column=000133120 Fraction=81.2% Gflops=1.971e+03
Column=000133376 Fraction=81.4% Gflops=1.971e+03
Column=000133632 Fraction=81.6% Gflops=1.971e+03
Column=000133888 Fraction=81.7% Gflops=1.971e+03
Column=000134144 Fraction=81.9% Gflops=1.971e+03
Column=000134400 Fraction=82.0% Gflops=1.971e+03
Column=000134656 Fraction=82.2% Gflops=1.971e+03
Column=000134912 Fraction=82.3% Gflops=1.971e+03
Column=000135168 Fraction=82.5% Gflops=1.971e+03
Column=000135424 Fraction=82.7% Gflops=1.971e+03
Column=000135680 Fraction=82.8% Gflops=1.970e+03
Column=000135936 Fraction=83.0% Gflops=1.970e+03
Column=000136192 Fraction=83.1% Gflops=1.970e+03
Column=000136448 Fraction=83.3% Gflops=1.970e+03
Column=000136704 Fraction=83.4% Gflops=1.970e+03
Column=000136960 Fraction=83.6% Gflops=1.970e+03
Column=000137216 Fraction=83.8% Gflops=1.970e+03
Column=000137472 Fraction=83.9% Gflops=1.970e+03
Column=000137728 Fraction=84.1% Gflops=1.970e+03
Column=000137984 Fraction=84.2% Gflops=1.970e+03
Column=000138240 Fraction=84.4% Gflops=1.970e+03
Column=000138496 Fraction=84.5% Gflops=1.970e+03
Column=000138752 Fraction=84.7% Gflops=1.970e+03
Column=000139008 Fraction=84.8% Gflops=1.970e+03
Column=000139264 Fraction=85.0% Gflops=1.970e+03
Column=000139520 Fraction=85.2% Gflops=1.970e+03
Column=000139776 Fraction=85.3% Gflops=1.970e+03
Column=000140032 Fraction=85.5% Gflops=1.970e+03
Column=000140288 Fraction=85.6% Gflops=1.970e+03
Column=000140544 Fraction=85.8% Gflops=1.970e+03
Column=000140800 Fraction=85.9% Gflops=1.970e+03
Column=000141056 Fraction=86.1% Gflops=1.970e+03
Column=000141312 Fraction=86.2% Gflops=1.970e+03
Column=000141568 Fraction=86.4% Gflops=1.970e+03
Column=000141824 Fraction=86.6% Gflops=1.970e+03
Column=000142080 Fraction=86.7% Gflops=1.970e+03
Column=000142336 Fraction=86.9% Gflops=1.970e+03
Column=000142592 Fraction=87.0% Gflops=1.970e+03
Column=000142848 Fraction=87.2% Gflops=1.970e+03
Column=000143104 Fraction=87.3% Gflops=1.970e+03
Column=000143360 Fraction=87.5% Gflops=1.970e+03
Column=000143616 Fraction=87.7% Gflops=1.970e+03
Column=000143872 Fraction=87.8% Gflops=1.969e+03
Column=000144128 Fraction=88.0% Gflops=1.969e+03
Column=000144384 Fraction=88.1% Gflops=1.969e+03
Column=000144640 Fraction=88.3% Gflops=1.969e+03
Column=000144896 Fraction=88.4% Gflops=1.969e+03
Column=000145152 Fraction=88.6% Gflops=1.969e+03
Column=000145408 Fraction=88.8% Gflops=1.969e+03
Column=000145664 Fraction=88.9% Gflops=1.969e+03
Column=000145920 Fraction=89.1% Gflops=1.969e+03
Column=000146176 Fraction=89.2% Gflops=1.969e+03
Column=000146432 Fraction=89.4% Gflops=1.969e+03
Column=000146688 Fraction=89.5% Gflops=1.969e+03
Column=000146944 Fraction=89.7% Gflops=1.969e+03
Column=000147200 Fraction=89.8% Gflops=1.969e+03
Column=000147456 Fraction=90.0% Gflops=1.969e+03
Column=000147712 Fraction=90.2% Gflops=1.969e+03
Column=000147968 Fraction=90.3% Gflops=1.969e+03
Column=000148224 Fraction=90.5% Gflops=1.969e+03
Column=000148480 Fraction=90.6% Gflops=1.969e+03
Column=000148736 Fraction=90.8% Gflops=1.969e+03
Column=000148992 Fraction=90.9% Gflops=1.969e+03
Column=000149248 Fraction=91.1% Gflops=1.969e+03
Column=000149504 Fraction=91.2% Gflops=1.969e+03
Column=000149760 Fraction=91.4% Gflops=1.969e+03
Column=000150016 Fraction=91.6% Gflops=1.969e+03
Column=000150272 Fraction=91.7% Gflops=1.969e+03
Column=000150528 Fraction=91.9% Gflops=1.969e+03
Column=000150784 Fraction=92.0% Gflops=1.969e+03
Column=000151040 Fraction=92.2% Gflops=1.969e+03
Column=000151296 Fraction=92.3% Gflops=1.969e+03
Column=000151552 Fraction=92.5% Gflops=1.969e+03
Column=000151808 Fraction=92.7% Gflops=1.969e+03
Column=000152064 Fraction=92.8% Gflops=1.969e+03
Column=000152320 Fraction=93.0% Gflops=1.968e+03
Column=000152576 Fraction=93.1% Gflops=1.968e+03
Column=000152832 Fraction=93.3% Gflops=1.968e+03
Column=000153088 Fraction=93.4% Gflops=1.968e+03
Column=000153344 Fraction=93.6% Gflops=1.968e+03
Column=000153600 Fraction=93.8% Gflops=1.968e+03
Column=000153856 Fraction=93.9% Gflops=1.968e+03
Column=000154112 Fraction=94.1% Gflops=1.968e+03
Column=000154368 Fraction=94.2% Gflops=1.968e+03
Column=000154624 Fraction=94.4% Gflops=1.968e+03
Column=000154880 Fraction=94.5% Gflops=1.968e+03
Column=000155136 Fraction=94.7% Gflops=1.968e+03
Column=000155392 Fraction=94.8% Gflops=1.968e+03
Column=000155648 Fraction=95.0% Gflops=1.968e+03
Column=000155904 Fraction=95.2% Gflops=1.968e+03
Column=000156160 Fraction=95.3% Gflops=1.968e+03
Column=000156416 Fraction=95.5% Gflops=1.968e+03
Column=000156672 Fraction=95.6% Gflops=1.968e+03
Column=000156928 Fraction=95.8% Gflops=1.968e+03
Column=000157184 Fraction=95.9% Gflops=1.968e+03
Column=000157440 Fraction=96.1% Gflops=1.968e+03
Column=000157696 Fraction=96.2% Gflops=1.968e+03
Column=000157952 Fraction=96.4% Gflops=1.968e+03
Column=000158208 Fraction=96.6% Gflops=1.968e+03
Column=000158464 Fraction=96.7% Gflops=1.968e+03
Column=000158720 Fraction=96.9% Gflops=1.968e+03
Column=000158976 Fraction=97.0% Gflops=1.968e+03
Column=000159232 Fraction=97.2% Gflops=1.968e+03
Column=000159488 Fraction=97.3% Gflops=1.968e+03
Column=000159744 Fraction=97.5% Gflops=1.968e+03
Column=000160000 Fraction=97.7% Gflops=1.968e+03
Column=000160256 Fraction=97.8% Gflops=1.968e+03
Column=000160512 Fraction=98.0% Gflops=1.968e+03
Column=000160768 Fraction=98.1% Gflops=1.968e+03
Column=000161024 Fraction=98.3% Gflops=1.968e+03
Column=000161280 Fraction=98.4% Gflops=1.968e+03
Column=000161536 Fraction=98.6% Gflops=1.968e+03
Column=000161792 Fraction=98.8% Gflops=1.968e+03
Column=000162048 Fraction=98.9% Gflops=1.968e+03
Column=000162304 Fraction=99.1% Gflops=1.968e+03
Column=000162560 Fraction=99.2% Gflops=1.968e+03
Column=000162816 Fraction=99.4% Gflops=1.968e+03
Column=000163072 Fraction=99.5% Gflops=1.968e+03
Column=000163328 Fraction=99.7% Gflops=1.968e+03
Column=000163584 Fraction=99.8% Gflops=1.968e+03
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2C4      163840   256     7     8            1490.59             1.9671e+03
HPL_pdgesv() start time Mon Feb 11 22:41:19 2019

HPL_pdgesv() end time   Mon Feb 11 23:06:10 2019

--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV-
Max aggregated wall time rfact . . . :              54.90
+ Max aggregated wall time pfact . . :              53.27
+ Max aggregated wall time mxswp . . :              52.84
Max aggregated wall time update  . . :            1434.55
+ Max aggregated wall time laswp . . :              63.63
Max aggregated wall time up tr sv  . :               0.51
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=   1.06227654e-03 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2C4           0   256     7     8               0.00             0.0000e+00
HPL_pdgesv() start time Mon Feb 11 23:06:24 2019

HPL_pdgesv() end time   Mon Feb 11 23:06:24 2019

--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV-
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=   0.00000000e+00 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2C4           0   256     7     8               0.00             0.0000e+00
HPL_pdgesv() start time Mon Feb 11 23:06:24 2019

HPL_pdgesv() end time   Mon Feb 11 23:06:24 2019

--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV--VVV-
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=   0.00000000e+00 ...... PASSED
================================================================================

Finished      3 tests with the following results:
              3 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
