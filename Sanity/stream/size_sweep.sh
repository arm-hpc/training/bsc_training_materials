#!/bin/bash

if grep -q GenuineIntel /proc/cpuinfo ; then
  TARGET=stream.icc
  CORES=56
else
  TARGET=stream.arm
  CORES=64
fi


i=10
imax=34
while [ $i -le $imax ] ; do
  ((STREAM_ARRAY_SIZE=2**$i))

  make clean $TARGET STREAM_ARRAY_SIZE=${STREAM_ARRAY_SIZE}ULL
  OMP_NUM_THREADS=$CORES OMP_PLACES=threads OMP_PROC_BIND=spread OMP_DISPLAY_ENV=verbose KMP_AFFINITY=verbose ./$TARGET
  
  ((i++))
done

