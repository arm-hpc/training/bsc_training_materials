module use /soft/compilers/ARM/19.0/opt/arm/modulefiles/
module load ThunderX2CN99/RHEL/7/arm-hpc-compiler-19.0/armpl/19.0.0

module use /soft/emulators/ARM/18.4/opt/arm/modulefiles/
module load Generic-AArch64/RHEL/7/arm-instruction-emulator/18.4

export PATH=/soft/libraries/mpi/mvapich2-2.3/arm-19.0/bin:$PATH
