program main
  implicit none

  integer, parameter :: DATA_LEN = 1024*1024

  real, dimension(DATA_LEN) :: x, a, b
  integer :: i

  x(:) = 0
  a(:) = 1
  b(:) = 2

  call vec_add(DATA_LEN, x, a, b);

  do i=1,DATA_LEN
    if (x(i) /= 3) then
      write (*,*) "ERROR: "
      stop
    end if
  end do
  write (*,*) "OK"

contains

  subroutine vec_add(N, x, a, b)
    implicit none
    integer, intent(in) :: N
    real, dimension(N), intent(out) :: x
    real, dimension(N), intent(in)  :: a, b

    x = a + b
  end subroutine vec_add

end program main
