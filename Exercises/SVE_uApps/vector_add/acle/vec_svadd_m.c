#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <arm_sve.h>

#define DATA_LEN (1024*1024)


void vec_svadd_m(size_t N, float * x, float * a, float * b)
{
  // Stride by the number of words in the vector
  for (size_t i=0; i<N; i+=svcntw()) {
    // Operate on vector lanes where i < N
    svbool_t pred = svwhilelt_b32(i, N);
    // Load a vector of a
    svfloat32_t sva = svld1(pred, a+i);
    // Load a vector of b
    svfloat32_t svb = svld1(pred, b+i);
    // Add a to b
    svfloat32_t svx = svadd_m(pred, sva, svb);
    // Store a+b 
    svst1(pred, x+i, svx);
  }
}


int main(int argc, char ** argv)
{
  float * x = (float*)malloc(DATA_LEN*sizeof(float));
  float * a = (float*)malloc(DATA_LEN*sizeof(float));
  float * b = (float*)malloc(DATA_LEN*sizeof(float));

  for (size_t i=0; i<DATA_LEN; ++i) {
    x[i] = 0;
    a[i] = 1;
    b[i] = 2;
  }

  vec_svadd_m(DATA_LEN, x, a, b);

  for (size_t i=0; i<DATA_LEN; ++i) {
    if (x[i] != 3) {
      printf("ERROR: x[%zd] = %g\n", i, x[i]);
      return 1;
    }
  }
  printf("OK\n");

  return EXIT_SUCCESS;
}
