#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define DATA_LEN (1024*1024)


void vec_add(size_t N, float * out, float * a, float * b)
{
  for (size_t i=0; i<N; ++i) {
    out[i] = a[i] + b[i];
  }
}


int main(int argc, char ** argv)
{
  float * x = (float*)malloc(DATA_LEN*sizeof(float));
  float * a = (float*)malloc(DATA_LEN*sizeof(float));
  float * b = (float*)malloc(DATA_LEN*sizeof(float));

  for (size_t i=0; i<DATA_LEN; ++i) {
    x[i] = 0;
    a[i] = 1;
    b[i] = 2;
  }

  vec_add(DATA_LEN, x, a, b);

  for (size_t i=0; i<DATA_LEN; ++i) {
    if (x[i] != 3) {
      printf("ERROR: x[%zd] = %g\n", i, x[i]);
      return 1;
    }
  }
  printf("OK\n");

  return EXIT_SUCCESS;
}
