#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <arm_sve.h>

#define DATA_LEN (1024*1024)


void vec_svadd_m(size_t N, float * out, float * a, float * b)
{
  // Initialize the predicate to all true
  svbool_t pred = svptrue_b32();

  // Prefetch the 0th vector of a into L1 with streaming semantics
  svprfw(pred, a, SV_PLDL1STRM);
  // Prefetch the 1st vector of a into L1 with streaming semantics
  svprfw_vnum(pred, a, 1, SV_PLDL1STRM);
  // Prefetch the 2nd vector of a into L1 with streaming semantics
  svprfw_vnum(pred, a, 2, SV_PLDL1STRM);

  // Prefetch the 0th vector of b into L1 with streaming semantics
  svprfw(pred, b, SV_PLDL1STRM);
  // Prefetch the 1st vector of b into L1 with streaming semantics
  svprfw_vnum(pred, b, 1, SV_PLDL1STRM);
  // Prefetch the 2nd vector of b into L1 with streaming semantics
  svprfw_vnum(pred, b, 2, SV_PLDL1STRM);

  // Stride by the number of words in the vector
  for (size_t i=0; i<N; i+=svcntw()) {
    // Operate on vector lanes for i < N
    pred = svwhilelt_b32(i, N);

    // Load vectors from `a` and `b`
    svfloat32_t sva = svld1(pred, a+i);
    svfloat32_t svb = svld1(pred, b+i);

    // Prefetch the 3rd vector of a into L1 with streaming semantics
    svprfw_vnum(pred, a+i, 3, SV_PLDL1STRM);
    // Prefetch the 3rd vector of b into L1 with streaming semantics
    svprfw_vnum(pred, b+i, 3, SV_PLDL1STRM);

    svfloat32_t svx = svadd_m(pred, sva, svb);
    svst1(pred, out+i, svx);
  }
}


int main(int argc, char ** argv)
{
  float * x = (float*)malloc(DATA_LEN*sizeof(float));
  float * a = (float*)malloc(DATA_LEN*sizeof(float));
  float * b = (float*)malloc(DATA_LEN*sizeof(float));

  for (size_t i=0; i<DATA_LEN; ++i) {
    x[i] = 0;
    a[i] = 1;
    b[i] = 2;
  }

  vec_svadd_m(DATA_LEN, x, a, b);

  for (size_t i=0; i<DATA_LEN; ++i) {
    if (x[i] != 3) {
      printf("ERROR: x[%zd] = %g\n", i, x[i]);
      return 1;
    }
  }
  printf("OK\n");

  return EXIT_SUCCESS;
}
