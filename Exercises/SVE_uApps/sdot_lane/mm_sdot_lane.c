#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <arm_sve.h>

void matmul_dotp_fixp(
        uint64_t M, uint64_t K, uint64_t N, 
        uint8_t * inLeft_MOD, 
        uint8_t * inRight_MOD, 
        uint32_t * out) 
{
    svuint32_t acc0, acc1, acc2, acc3;
    uint64_t offsetOUT_1, offsetOUT_2, offsetOUT_3;
    uint8_t * ptrIN_left;
    uint8_t * ptrIN_right;
    uint32_t * ptrOUT;

    svbool_t p8_all = svptrue_b8();
    uint64_t vl = svcntb();
    uint64_t vl_4 = svcntw();    // vl/4
    offsetOUT_1 = N;
    offsetOUT_2 = 2*N;
    offsetOUT_3 = 3*N;

    for (uint64_t x=0; x<M; x+=4) {
        ptrOUT = &out[x*N];
        ptrIN_right = &inRight_MOD[0];

        for (uint64_t y=0; y<N; y+=vl_4) {
            ptrIN_left = &inLeft_MOD[x*K];
            acc0 = svdup_u32(0);
            acc1 = svdup_u32(0);
            acc2 = svdup_u32(0);
            acc3 = svdup_u32(0);

            for (uint64_t z=0; z<K; z+=4) {
                svuint8_t a0123 = svld1rq(p8_all, ptrIN_left);
                svuint8_t b_vec = svld1(p8_all, ptrIN_right);
                acc0 = svdot_lane(acc0, b_vec, a0123, 0);
                acc1 = svdot_lane(acc1, b_vec, a0123, 1);
                acc2 = svdot_lane(acc2, b_vec, a0123, 2);
                acc3 = svdot_lane(acc3, b_vec, a0123, 3);
                ptrIN_left += 16;
                ptrIN_right += vl;
            }

            svst1(p8_all, ptrOUT, acc0);
            svst1(p8_all, &ptrOUT[offsetOUT_1], acc1);
            svst1(p8_all, &ptrOUT[offsetOUT_2], acc2);
            svst1(p8_all, &ptrOUT[offsetOUT_3], acc3);
            ptrOUT += vl_4;
        }
    }
}

int main(int argc, char ** argv)
{

}

