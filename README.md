BSC Hackathon Training Materials
================================

Summary
-------

TBD

Contents
--------

  - `arm_env.bashrc` and `intel_env.bashrc`.
        Update as needed.
  - `Data`: Reference benchmark data.
  - `Exercises`: Hands-on exercises.
    - `ArmIE_Tutorial_Exercises`: ArmIE walk-through with HACC
    - `Compiler_Tutorials`: Matrix multiplication done seven ways.  See README.
    - `Forge_Tutorials`: Hands-on matierals for Arm Forge
    - `SVE_uApps`: Hands-on materials for SVE and ACLE.
  - `Sanity`: Useful benchmarks for checking system performance.
    - `hpcg`: HPCG with input files scaled for a ThunderX2 node.
    - `hpl-2.3`: HPL with input files scaled for a ThunderX2 node.
    - `stream`: STREAM modified to generate CSV file and sweep through array sizes.
  - `Slides`: Slides!  What did you expect?
  - `Topologies`: System topologies from `lstopo`

Credits
-------

The slides and code examples were created by many people including _at least_:
  * John C. Linford <john.linford@arm.com>
  * Olly Perks
  * Chris Goodyer
  * Will Lovett
  * Ashok Bhat
  * Francesco Petrogalli
  * Roxana Rusitoru
  * Daniel Ruiz Munoz
  * Miguel Tairum-Cruz
  * Jelena Milanovic
